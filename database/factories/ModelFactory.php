<?php

use Sizo0\RBAC\Eloquent\Permission;
use Sizo0\RBAC\Eloquent\Role;
use Sizo0\RBAC\Eloquent\User;

$factory->define(User::class, function (Faker\Generator $faker) {
    return [
        'email'    => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
    ];
});

$factory->define(Role::class, function (Faker\Generator $faker) {
    return [
        'slug'        => $faker->slug(1),
        'description' => $faker->paragraph,
    ];
});

$factory->define(Permission::class, function (Faker\Generator $faker) {
    return [
        'slug'        => $faker->slug(2),
        'description' => $faker->paragraph,
    ];
});
