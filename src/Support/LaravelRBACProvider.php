<?php

namespace Sizo0\RBAC\Support;


use Illuminate\Support\ServiceProvider;

class LaravelRBACProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([__DIR__ . '/../../database/migrations/' => database_path('migrations')], 'migrations');
    }
}