<?php

namespace Sizo0\RBAC\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RoleVerifier
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @param array                     $roles
     *
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        $user = $request->user();

        if (!$user->is($roles)) {
            return response('Forbidden.', 403);
        }

        return $next($request);
    }
}
