<?php

namespace Sizo0\RBAC\Eloquent;


use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = ['slug', 'description'];
}