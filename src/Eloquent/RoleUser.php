<?php

namespace Sizo0\RBAC\Eloquent;

use Illuminate\Database\Eloquent\Relations\Pivot;

class RoleUser extends Pivot
{
    protected $dates = ['start_time', 'end_time'];
}
