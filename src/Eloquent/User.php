<?php

namespace Sizo0\RBAC\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    protected $fillable = ['email', 'password'];
    protected $hidden   = ['password'];

    public function newPivot(Model $parent, array $attributes, $table, $exists)
    {
        if ($parent instanceof Role) {
            return new RoleUser($parent, $attributes, $table, $exists);
        }
        return parent::newPivot($parent, $attributes, $table, $exists);
    }

    /**
     * Check if a user has a role or multiple roles
     * If all is true all the user will have to have all the roles
     * Otherwise the user will have to have at least one role
     *
     * @param array|mixed $slug_roles
     * @param bool        $all
     *
     * @return bool
     */
    public function is($slug_roles, $all = false)
    {
        $slug_roles = is_array($slug_roles) ? $slug_roles : [$slug_roles];

        $count_roles = $this->roles()->whereIn('slug', $slug_roles)->count();

        return $all ? $count_roles === count($slug_roles) : $count_roles > 0;
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class)->withPivot(['start_time', 'end_time']);
    }
}
