<?php

namespace Sizo0\RBAC\Tests\Http\Middleware;


use Sizo0\RBAC\Eloquent\Role;
use Sizo0\RBAC\Eloquent\User;
use Sizo0\RBAC\Tests\TestCase;

class RoleVerifierTest extends TestCase
{
    public function testOneRoleAuthorize()
    {
        $user = factory(User::class)->create();
        $role = factory(Role::class)->create(['slug' => 'a']);
        $user->roles()->attach($role->id);

        $this->be($user);

        $this->call('GET', '/a');

        $this->assertResponseOk();
    }

    public function testOneRoleDeny()
    {
        $user = factory(User::class)->create();
        $role = factory(Role::class)->create(['slug' => 'b']);
        $user->roles()->attach($role->id);

        $this->be($user);

        $this->call('GET', '/a');

        $this->assertResponseStatus(403);
    }

    public function testMultipleRolesAndAuthorize()
    {
        $user = factory(User::class)->create();
        $role1 = factory(Role::class)->create(['slug' => 'a']);
        $role2 = factory(Role::class)->create(['slug' => 'b']);
        $user->roles()->attach([$role1->id, $role2->id]);

        $this->be($user);

        $this->call('GET', '/a-and-b');

        $this->assertResponseOk();
    }

    public function testMultipleRolesAndDeny()
    {
        $user = factory(User::class)->create();
        $role = factory(Role::class)->create(['slug' => 'a']);
        $user->roles()->attach([$role->id]);

        $this->be($user);

        $this->call('GET', '/a-and-b');

        $this->assertResponseStatus(403);
    }

    public function testMultipleRolesOrAuthorize()
    {
        $user = factory(User::class)->create();
        $role = factory(Role::class)->create(['slug' => 'a']);
        $user->roles()->attach([$role->id]);

        $this->be($user);

        $this->call('GET', '/a-or-b');

        $this->assertResponseOk();
    }

    public function testMultipleRolesOrDeny()
    {
        $user = factory(User::class)->create();
        $role1 = factory(Role::class)->create(['slug' => 'c']);
        $role2 = factory(Role::class)->create(['slug' => 'd']);
        $user->roles()->attach([$role1->id, $role2->id]);

        $this->be($user);

        $this->call('GET', '/a-or-b');

        $this->assertResponseStatus(403);
    }

    public function testConjunctionRolesAuthorize()
    {
        $user = factory(User::class)->create();
        $role1 = factory(Role::class)->create(['slug' => 'a']);
        $role2 = factory(Role::class)->create(['slug' => 'd']);
        $user->roles()->attach([$role1->id, $role2->id]);

        $this->be($user);

        $this->call('GET', '/a-or-b-and-c-or-d');

        $this->assertResponseOk();
    }

    public function testConjunctionRolesDeny()
    {
        $user = factory(User::class)->create();
        $role1 = factory(Role::class)->create(['slug' => 'a']);
        $role2 = factory(Role::class)->create(['slug' => 'b']);
        $user->roles()->attach([$role1->id, $role2->id]);

        $this->be($user);

        $this->call('GET', '/a-or-b-and-c-or-d');

        $this->assertResponseStatus(403);
    }

    protected function getEnvironmentSetUp($app)
    {
        require __DIR__ . '/../routes.php';
    }
}