<?php

use Illuminate\Support\Facades\Route;

Route::get('/a', function () {
    return view('welcome');
})->middleware(['role:a']);

Route::get('/a-and-b', function () {
    return view('welcome');
})->middleware(['role:a', 'role:b']);

Route::get('/a-or-b', function () {
    return view('welcome');
})->middleware(['role:a,b']);

Route::get('/a-or-b-and-c-or-d', function () {
    return view('welcome');
})->middleware(['role:a,b', 'role:c,d']);
