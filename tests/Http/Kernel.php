<?php

namespace Sizo0\RBAC\Tests\Http;

use Orchestra\Testbench\Http\Kernel as HttpKernel;
use Sizo0\RBAC\Http\Middleware\RoleVerifier;

class Kernel extends HttpKernel
{
    protected $routeMiddleware = [
        'auth'       => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'can'        => \Illuminate\Foundation\Http\Middleware\Authorize::class,
        'guest'      => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle'   => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'role'         => RoleVerifier::class,
    ];
}
