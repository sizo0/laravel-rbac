<?php

namespace Sizo0\RBAC\Tests\Eloquent;

use Carbon\Carbon;
use Sizo0\RBAC\Eloquent\Permission;
use Sizo0\RBAC\Eloquent\Role;
use Sizo0\RBAC\Eloquent\User;
use Sizo0\RBAC\Tests\TestCase;

class UserTest extends TestCase
{
    public function testIsForOneRoleTrue()
    {
        $permission = factory(Permission::class)->create();

        $role = factory(Role::class)->create();
        $role->permissions()->attach($permission->id);

        $user = factory(User::class)->create();
        $start_time = Carbon::now();
        $end_time = Carbon::now();
        $end_time->addSeconds(mt_rand(3600, 3600 * 24 * 365));
        $user->roles()->attach([$role->id => ['start_time' => $start_time, 'end_time' => $end_time]]);

        $this->assertTrue($user->is($role->slug));
    }

    public function testIsForOneRoleFalse()
    {
        $faker = app('Faker\Generator');
        $permission = factory(Permission::class)->create();

        $role = factory(Role::class)->create();
        $role->permissions()->attach($permission->id);

        $user = factory(User::class)->create();
        $start_time = Carbon::now();
        $end_time = Carbon::now();
        $end_time->addSeconds(mt_rand(3600, 3600 * 24 * 365));
        $user->roles()->attach([$role->id => ['start_time' => $start_time, 'end_time' => $end_time]]);

        $this->assertFalse($user->is($faker->slug));
    }

    public function testIsForMultipleRolesTrue()
    {
        $user = factory(User::class)->create();

        $permission1 = factory(Permission::class)->create();
        $permission2 = factory(Permission::class)->create();

        $role1 = factory(Role::class)->create();
        $role2 = factory(Role::class)->create();
        $role1->permissions()->attach([$permission1->id, $permission2->id]);
        $role2->permissions()->attach($permission2->id);

        $start_time = Carbon::now();
        $end_time = Carbon::now();
        $end_time->addSeconds(mt_rand(3600, 3600 * 24 * 365));

        $user->roles()->attach([$role1->id => ['start_time' => $start_time, 'end_time' => $end_time]]);

        $this->assertTrue($user->is([$role1->slug, $role2->slug]));
    }

    public function testIsForMultipleRolesAllFalse()
    {
        $user = factory(User::class)->create();

        $permission1 = factory(Permission::class)->create();
        $permission2 = factory(Permission::class)->create();

        $role1 = factory(Role::class)->create();
        $role2 = factory(Role::class)->create();
        $role1->permissions()->attach([$permission1->id, $permission2->id]);
        $role2->permissions()->attach($permission2->id);

        $start_time = Carbon::now();
        $end_time = Carbon::now();
        $end_time->addSeconds(mt_rand(3600, 3600 * 24 * 365));

        $user->roles()->attach([$role1->id => ['start_time' => $start_time, 'end_time' => $end_time]]);

        $this->assertFalse($user->is([$role1->slug, $role2->slug], true));
    }
}